@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">{{ __('Create User') }}</div>
					<div class="card-body">
						<form method="POST" action="{{ url('/users/create/new') }}">
							@csrf
							<div class="form-group">
								<label for="first_name" class="form-label">{{ __('First Name') }}</label>

								<input id="first_name" type="text"
								       class="form-control @error('first_name') is-invalid @enderror"
								       name="first_name" value="{{ old('first_name') }}" required autocomplete="name"
								       autofocus>

								@error('first_name')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
                                </span>
								@enderror
							</div>

							<div class="form-group">
								<label for="name" class="form-label">{{ __('Last Name') }}</label>

								<input id="last_name" type="text"
								       class="form-control @error('last_name') is-invalid @enderror"
								       name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name"
								       autofocus>

								@error('last_name')
								<span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
								@enderror
							</div>

							<div class="form-group">
								<label for="email" class="form-label">{{ __('E-Mail Address') }}</label>

								<input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
								       name="email"
								       value="{{ old('email') }}" required autocomplete="email">

								@error('email')
								<span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
								@enderror
							</div>

							<div class="form-group">
								<label for="password" class="form-label">{{ __('Password') }}</label>

								<input id="password" type="password"
								       class="form-control @error('password') is-invalid @enderror"
								       name="password" required autocomplete="new-password">

								@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
                                </span>
								@enderror
							</div>

							<div class="form-group">
								<label for="password-confirm" class="form-label">{{ __('Confirm Password') }}</label>

								<input id="password-confirm" type="password" class="form-control"
								       name="password_confirmation" required
								       autocomplete="new-password">
							</div>

							<div class="form-group mb-0">
								<button type="submit" class="btn btn-primary">
									{{ __('Create User') }}
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection