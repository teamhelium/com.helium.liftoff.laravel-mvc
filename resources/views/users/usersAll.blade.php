@extends('layouts.app')

@section('content')
	<div class="container-fluid">
		{{-- Filters --}}
		<div class="row">
			{{-- User Grid --}}
			<div class="col-12 col-lg-10">
				<div class="row">
					@foreach($users as $user)
						<div class="col-12 col-sm-6 col-xl-4">
							<a href="{{"users/{$user->id}/profile"}}">{{$user->first_name}} {{$user->last_name}}</a>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection