@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-12 col-xl-6 offset-xl-3">
			<form action="{{"/users/{$user->id}"}}" method="POST" enctype="multipart/form-data">
				@csrf
				@method('PATCH')
				<div class="d-flex flex-column mb-3 ">
					<h1 class="h2">My Profile</h1>
					<div class="d-flex flex-row align-items-center justify-content-between">
						<button type="button" data-toggle="modal" data-target="#passwordModal" class="btn btn-danger">Change
							Password</button>
					</div>
				</div>
				<div class="card  mb-3">
					<div class="card-header">
						<h3 class="m-0">User Information</h3>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label class="form-label">First Name:</label>
							<input class="form-control" type="text" name="first_name" value="{{$user->first_name}}">
						</div>
						<div class="form-group">
							<label class="form-label">Last Name:</label>
							<input class="form-control" type="text" name="last_name" value="{{$user->last_name}}">
						</div>
						<div class="form-group">
							<label class="form-label">Email:</label>
							<input class="form-control" type="text" name="status" value="{{$user->email}}">
						</div>
					</div>
				</div>

				<div class="mt-4 d-flex flex-row">
					<button type="button" class="btn btn-danger mr-2">Cancel</button>
					<button type="submit" class="btn btn-success mr-2">Save</button>
					@if($user->deleted_at == null)
						<button data-toggle="modal" data-target="#deleteUserModal" type="button" class="btn btn-danger mr-2">Delete
							User</button>
					@else
						<button data-toggle="modal" data-target="#activateUserModal" type="button" class="btn btn-success mr-2">Activate User</button>
					@endif
				</div>
			</form>
			@include('modals.updatePassword')
			@include('modals.deleteUser')
			@include('modals.activateUser')
		</div>
	</div>
@endsection
