<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body px-0 pt-4 pb-0" style="color: black;">
				<form action="{{url("/users/{$user->id}/delete-user")}}" method="POST" class="col-12 px-4">
					@csrf
					@method('DELETE')
					<h1 class="">Are you sure you want to delete this User?</h1>
					<div class="">
						<div class="">
							<button type="submit" class="">
								Delete User
							</button>
						</div>
						<div class="">
							<button type="button" class="" data-dismiss="modal">
								Cancel
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>