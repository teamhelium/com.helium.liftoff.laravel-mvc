<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h2 class="h5 modal-title">Update Your Password</h2>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
			</div>

			<form action="{{url("/users/{$user->id}/update-password")}}" method="POST">

				<div class="modal-body" style="color: black;">
					@csrf
					@method('PATCH')
					<div class="form-group">
						<label for="password" class="form-label">Password</label>
						<input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
						       name="password" required autocomplete="new-password">

						@error('password')
						<span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
						@enderror
					</div>

					<div class="form-group">
						<label for="password-confirm" class="form-label">Confirm Password</label>
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
						       autocomplete="new-password">
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">
						{{ __('Reset Password') }}
					</button>
				</div>

			</form>
		</div>
	</div>
</div>