<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Email Verification Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling email verification for any
	| user that recently registered with the application. Emails may also
	| be re-sent if the user didn't receive the original email message.
	|
	*/

	use VerifiesEmails;

	/**
	 * Where to redirect users after verification.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth')->except('verify');
		$this->middleware('signed')->only('verify');
		$this->middleware('throttle:6,1')->only('verify', 'resend');
	}

	public function verify(Request $request)
	{
		$userId = $request->route('id');
		$user = User::findOrFail($userId);

		if ($request->user()) {
			Auth::logout();
		}

		if ($user->hasVerifiedEmail()) {
			session()->flash('error', 'User has already been verified.');

			return redirect('/');
		}

		if ($user->markEmailAsVerified()) {
			event(new Verified($user));
			session()->put('password_verify_user_id', $userId);

			return redirect('password/set');
		}

		return redirect('/');
	}
}
