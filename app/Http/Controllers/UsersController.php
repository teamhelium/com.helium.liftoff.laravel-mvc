<?php

namespace App\Http\Controllers;

use App\Enums\UserCreateType;
use App\Models\Primary\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
	public function index()
	{
		$users = User::withTrashed()->get();

		return view('users.usersAll', ['users' => $users]);
	}

	public function show(Request $request, $id)
	{
		$user = User::withTrashed()->where('id', $id)->first();

		return view('users.profile', ['user' => $user]);
	}

	public function create()
	{
		return view('users.newUser');
	}

	public function invite()
	{
		return view('users.newUserInvite');
	}

	public function setPassword(Request $request)
	{
		$userId = $request->session()->get('password_verify_user_id', null);

		if ($userId == null) {
			$request->session()->flash('error', 'Password Verification is not valid.');

			return redirect('/');
		} else {
			return view('auth.passwords.set');
		}
	}

	public function store(Request $request)
	{
		try {
			$user = User::createUser($request);

			if ($request->type == UserCreateType::NEW) {
				session()->flash('success', 'User Created!');

				return redirect("/users/{$user->id}/profile");
			} else {
				session()->flash('success', 'User Created!');

				return redirect('/users');
			}
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	public function update(Request $request, $id)
	{
		try {
			$user = User::updateUser($request, $id);

			if ($user) {
				return redirect("/users/{$user->id}/profile");
			}
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	public function destroy(Request $request, $id)
	{
		try {
			$user = User::withTrashed()->where('id', $id)->first();

			if ($user->delete()) {
				session()->flash('success', 'User Deleted.');

				return redirect('/users');
			} else {
				//this is just here in case there is a weird 500 error It should never happen but the event that it does
				session()->flash('error', 'User Cannot Be Deleted');

				return redirect("/users/{$user->id}/profile");
			}
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	public function storePassword(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'_token' => 'required|string',
			'password' => ['required', 'string', 'min:8', 'confirmed']
		]);

		if ($validator->fails()) {
			return response()->json(['error' => $validator->errors()], 400);
		}

		$userId = $request->session()->get('password_verify_user_id', null);

		if ($userId == null) {
			$request->session()->flash('error', 'Password Verification is not valid.');

			return redirect('/');
		} else {
			$user = User::find($userId);

			$user->update($request->all());

			if ($user->save()) {
				Auth::login($user);
				session()->remove('password_verify_user_id');

				return redirect('/home');
			} else {
				return redirect('/');
			}
		}
	}

	public function updatePassword(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'_token' => 'required|string',
			'password' => ['required', 'string', 'min:8', 'confirmed']
		]);

		if ($validator->fails()) {
			return response()->json(['error' => $validator->errors()], 400);
		}

		$user = User::find($id);

		$user->update($request->all());

		if ($user) {
			return redirect("/users/{$user->id}/profile");
		}
	}

	public function activate(Request $request, $id)
	{
		try {
			$user = User::activate($request, $id);

			if ($user) {
				return redirect("/users/{$user->id}/profile");
			}
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}
}
