<?php

namespace App\Enums;

use Konekt\Enum\Enum;

/**
 * @method static UserCreateType NEW()
 * @method static UserCreateType INVITE()
 */
class UserCreateType extends Enum
{
	const NEW = 'new';
	const INVITE = 'invite';
}