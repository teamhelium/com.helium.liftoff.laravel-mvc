<?php

namespace App\Models\Primary;

use App\Enums\UserCreateType;
use App\Notifications\UserAccountCreated;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
	use Notifiable;
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	protected function setPasswordAttribute($password)
	{
		if (!preg_match('/^\$2y\$.{56}$/', $password)) {
			$this->attributes['password'] = Hash::make($password);
		}

		$this->attributes['password'] = $password;
	}

	public static function createUser(Request $request)
	{
		$attributes = $request->all();

		if ($request->type == UserCreateType::INVITE) {
			$attributes['password'] = uniqid();
		}

		$user = User::create($attributes);

		if ($request->type == UserCreateType::INVITE) {
			$user->notify(new UserAccountCreated($user));
		}

		return $user;
	}

	public static function updateUser(Request $request, $id)
	{
		$user = User::find($id);

		if ($user) {
			$user->update($request->all());

			if ($request->photo_url) {
				$location = "/photos/{$user->id}/" . uniqid();
				$path = $request->file('photo_url')->store($location, 's3');

				if ($path) {
					$user->photo_url = $path;
				}
				$user->save();
			}

			return $user;
		} else {
			$exception = new \Exception('User Cannot Be Updated');

			throw $exception;
		}
	}

	public static function activate(Request $request, $id)
	{
		$user = User::withTrashed()->where('id', $id)->first();

		$user->restore();

		return $user;
	}
}
