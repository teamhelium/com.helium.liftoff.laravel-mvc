<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function()
{
	return view('welcome');
});

Route::get('/password/set', 'UsersController@setPassword')->name('password.set');
Route::post('/password/store', 'UsersController@storePassword')->name('password.store');

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth', 'verified', 'active']], function()
{
	Route::resource('users', 'UsersController');
	Route::get('/users/invite', 'UsersController@invite')
		->name('users.invite');
	Route::patch('/users/{user}/update-password', 'UsersController@updatePassword')
		->name('users.updatePassword');
	Route::patch('/users/{user}/activate', 'UsersController@activate')
		->name('users.activate');

	Route::get('/home', 'HomeController@index')->name('home');
});

